library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--this is a test!!!
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
entity alu is
Port ( clk : in  STD_LOGIC;
       reset : in  STD_LOGIC;
       inst_reg : in  STD_LOGIC_VECTOR (15 downto 0);
       AC : buffer  STD_LOGIC_VECTOR (15 downto 0));
end alu;
 
architecture Behavioral of alu is
signal address: STD_LOGIC_VECTOR (11 downto 0);
signal opCode: STD_LOGIC_VECTOR (2 downto 0);
signal I: STD_LOGIC;
begin
  process (reset)
    begin
      address <= inst_reg(11 downto 0);
      opCode <= inst_reg (14 downto 12);
      I <= inst_reg(15);
    end process;
process(Clk)
begin

    if(rising_edge(Clk)) then --Do the calculation at the positive edge of clock cycle.
        case opCode is
          when "000" => 
              --  with (I) select (AC) 
            when "001" => 
       
            when "010" => 
       
            when "011" => 
       
            when "100" => 
            when "101" => 
            when "110" => 
            when "111" => 
          
             case address is
             when "100000000000"=>
              
            when "010000000000"=>
              
            when "001000000000"=>

            when "000000010000"=>
            when "000000000001"=>
            
            when others =>
                NULL;

          end case; 
            
            when others =>
                NULL;
        end case;       
    end if;
end process;
end Behavioral;