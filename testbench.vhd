library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


ENTITY tb IS
END tb;

ARCHITECTURE testbench OF tb IS 

COMPONENT alu 
PORT(
      clk : in  STD_LOGIC;
       reset : in  STD_LOGIC;
       inst_reg : in  STD_LOGIC_VECTOR (15 downto 0);
       AC : buffer  STD_LOGIC_VECTOR (15 downto 0)
  );
END COMPONENT;
signal clk :  STD_LOGIC;
  signal     reset :  STD_LOGIC;
  signal     inst_reg : STD_LOGIC_VECTOR (15 downto 0);
  signal     AC :  STD_LOGIC_VECTOR (15 downto 0);

BEGIN
UUT: alu PORT MAP (
        clk => clk,
        reset => reset,
        inst_reg => inst_reg,
        AC=>AC
      );

PROCESS
BEGIN

END PROCESS;
END testbench;